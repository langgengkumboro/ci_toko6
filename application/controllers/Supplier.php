<?php
class Supplier extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Supplier_models");
	}

	public function index()
	{
		$this->data_supplier();
	}
	public function data_supplier()
	{
		// $data['data_supplier'] = $this->Supplier_models->tampilDataSupplier();
		
		// $data['content']	='forms/data_supplier';
		// $this->load->view('Home_2', $data);

		if (isset($_POST['cari_data'])) {
			$data['kata_pencarian'] = $this->input->post('cari_nama');
			$this->session->set_userdata('session_pencariansupplier', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] =$this->session->userdata('session_pencariansupplier');
		}
		// 	echo "<pre>";
		// print_r($this->session->userdata()); die();
		// echo "</pre>";

		$data['data_supplier']	= $this->Supplier_models->tombolpagination($data
			['kata_pencarian']);

		$data['content']	= 'forms/data_supplier';
		$this->load->view('Home_2', $data);
	}

	public function detailsupplier($kode_supplier)
	{
		$data['data_supplier'] =$this->Supplier_models->detailsupplier($kode_supplier);
		
		$data['content']	='forms/detailsupplier';
		$this->load->view('Home_2', $data);
	}
	public function inputsupplier()
	{
		$data['data_supplier'] = $this->Supplier_models->tampilDataSupplier();

		//if (!empty($_REQUEST)){
		//	$m_supplier = $this->Supplier_models;
		//	$m_supplier->save();
		//	redirect("Supplier/index", "refresh");
		//}
		$validation = $this->form_validation;
		$validation->set_rules($this->Supplier_models->rules());

		if ($validation->run()){
			$this->Supplier_models->save();
			$this->session->set_flashdata('info', '<div style="color : green">SIMPAN DATA BERHASIL HOREEE </div>');
			redirect("Supplier/index", "refresh");
			}
		

		$data['content']	='forms/inputsupplier';
		$this->load->view('Home_2', $data);
	}

	public function editsupplier($kode_supplier)
	{	
		$data['data_supplier']	= $this->Supplier_models->tampilDataSupplier($kode_supplier);
		
		//if (!empty($_REQUEST)) {
		//		$m_supplier = $this->Supplier_models;
		//		$m_supplier->update($kode_supplier);
		//		redirect("Supplier/index", "refresh");	
		//	}
		$validation = $this->form_validation;
		$validation->set_rules($this->Supplier_models->rules());

		if ($validation->run()){
			$this->Supplier_models->update($kode_supplier);
			$this->session->set_flashdata('info', '<div style="color : green">UPDATE DATA BERHASIL HOREEE </div>');
			redirect("Supplier/index", "refresh");
			}
		

		$data['content']	='forms/editsupplier';
		$this->load->view('Home_2', $data);
			
	}
	public function delete($kode_supplier)
	{
		$m_supplier = $this->Supplier_models;
		$m_supplier->delete($kode_supplier);	
		redirect("Supplier/index", "refresh");	
	}
	

}

