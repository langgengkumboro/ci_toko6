<body>
    <div style="color: red"><?=validation_errors(); ?></div>
    <div><center><h1>"Input Pembelian Detail"</h1></center></div>
    <form action="<?=base_url()?>Pembelian/inputDetail/<?= $id_header; ?>" method="POST">
<table width="46%" border="0" cellspacing="0" cellpadding="5" bgcolor="green">
 <tr>
        <td width="43%">Nama Barang</td>
        <td width="5%">:</td>
        <td width="52%">
            <select id="kode_barang" name="kode_barang">
                <?php foreach($data_barang as $data) { ?>
                <option value="<?= $data->kode_barang; ?>"><?= $data->nama_barang; ?></option>
                <?php }?>
            </select>
        </td>
    </tr>

<tr>
        <td width="43%">Qty</td>
        <td width="5%">:</td>
        <td width="52%">
            <input type="text" id="qty" name="qty">
        </td>
</tr>


<tr>
        <td width="43%">Harga Barang</td>
        <td width="5%">:</td>
        <td width="52%">
            <input type="text" id="harga" name="harga">
        </td>
    </tr>
 <tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="Submit" name="Submit" id="Submit" value="Proses" />
    </td>
  </tr>

<table width="100%" border="1" cellspacing="0" cellpadding="3">
    <tr align="center" style="background:#000; color:#FFF;">
        <td width="3%">No</td>
        <td width="15%">Kode Barang</td>
        <td width="22%">Nama Barang</td>
        <td width="20%">Qty</td>
        <td width="24%">Harga</td>
        <td width="24%">Jumlah</td>
    </tr>
    <?php
    // var_dump($data_pembelian_detail); die();
        $no = 0;
        $total_hitung = 0;
        foreach ($data_pembelian_detail as $data) {
            $no++;
    ?>
    <tr align="center">
        <td><?= $no; ?></td>
        <td><?= $data->kode_barang; ?></td>
        <td><?= $data->nama_barang; ?></td>
        <td><?= $data->qty; ?></td>
        <td>Rp. <?= number_format($data->harga); ?> ,-</td>
        <td>Rp. <?= number_format($data->jumlah); ?> ,-</td>
    </tr>
    <?php
        // hitung total
        $total_hitung += $data->jumlah;
        } 
    ?>
    <tr align="center">
        <td colspan="5" align="right"><b>TOTAL</b></td>
        <td  align="right">Rp. <b><?= number_format($total_hitung); ?></b></td>
    </tr>
    </table>
    <br/>
    <td>
         <input type="Submit" name="Submit" id="Submit" value="kembali ke Menu Sebelumnya" />
      <a href="<?= base_url(); ?>Pembelian/datapembelian"><font color="white">kembali ke Menu Sebelumnya</font></a>
  </tr>
</div>
</table>
</form>

</body>
