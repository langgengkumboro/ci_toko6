<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user_login = $this->session->userdata();
?>

<head>
    <title>TOKO JAYA ABADI</title>
    <marquee direction="right" style="animation-duration:1" aria-setsize="60" bgcolor="white" behavior="scroll" loop="0" >"ASometimes you have to love people from a distance and pray for him from your heart" | Butuh Website? Solusinya adalah <i><b>Jagooonya_Website.com</b></i></marquee>
    
    <link rel="stylesheet" href="<?=base_url();?>/assets/style.css" type="text/css" media="screen"/>
</head>
<?php
    $tgl=date('l, d-m-Y');
    echo $tgl;
?>
<meta charset="utf-8">
<title>Jam Digital</title>
<script type="text/javascript">
    window.setTimeout("waktu()",1000);
    function waktu() {
        var tanggal = new Date();
        setTimeout("waktu()",1000);
        document.getElementById("jam").innerHTML = tanggal.getHours();
        document.getElementById("menit").innerHTML = tanggal.getMinutes();
        document.getElementById("detik").innerHTML = tanggal.getSeconds();
    }
</script>
</head>

<style>
    #jam-digital{overflow:hidden; width:350px}
    #hours{float:left; width:100px; height:50px; background-color:#6B9AB8; margin-right:25px}
    #minute{float:left; width:100px; height:50px; background-color:#A5B1CB}
    #second{float:right; width:100px; height:50px; background-color:#FF618A; margin-left:25px}
    #jam-digital p{color:#FFF; font-size:36px; text-align:center; margin-top:4px}
</style>

<body onLoad="waktu()">
    <div id="jam-digital">
        <div id="hours"><p id="jam"></p></div>
        <div id="minute"><p id="menit"></p></div>
        <div id="second"><p id="detik"></p></div>
    </div>
<div>
  <center>
    <?php
      if ($user_login['tipe'] == "1"){
        $this->load->view('template/view_menu');
      }else{
        $this->load->view('template/view_menu_user');
      }
    ?>
  </center>
</div>
<?php $this->load->view($content);?>


 