<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "karyawan";

	public function rules(){
		return
		[
					[ 
						'field'=> 'nik',
						'label' => 'NIK',
						'rules' => 'required|numeric',
						'errors' => [
							'required' => 'NIK tidak Boleh kosong.',
							'numeric'	=> 'NIK harus angka.',
						]
					],
					[
						'field' => 'nama_karyawan',
						'label'  => 'Nama karyawan',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Nama karyawan tidak Boleh kosong.',
						]
					],
					[
						'field' => 'tempat_lahir',
						'label'  => 'Tempat Lahir',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Tempat Lahir tidak Boleh kosong.',
						]
					],
					
					[
						'field' => 'telpon',
						'label'  => 'Telpon',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Telpon tidak Boleh kosong.',
							
						]
					],
					[
						'field' => 'alamat',
						'label'  => 'Alamat',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Alamat tidak Boleh kosong.',
							
						]
					],
					//[
					//	'field' => 'kode_jabatan',
					//	'label'  => 'Jabatan',
					//	'rules' => 'required',
					//	'errors' =>[
					//		'required' => 'Jabatan tidak Boleh kosong.',
							
					//	]
					//]
					//[
					//	'field' => 'stok',
					//	'label'  => 'Stok Barang',
					//	'rules' => 'required|numeric',
					//	'errors' =>[
					//		'required' => 'Stok Barang tidak Boleh kosong.',
					//		'numeric'	=> 'Stok Barang harus angka.',
					//	]
					//]
		];
	}


	public function tampilDataKaryawan()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataKaryawan2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
			return $query->result();
		}

	public function tampilDataKaryawan3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('nik', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
	public function save()
		{
			$tgl 			= $this->input->post('tgl');
			$bulan 			= $this->input->post('bulan');
			$tahun 			= $this->input->post('tahun');
			$nik_karyawan	= $this->input->post('nik');
			$tgl_gabung 	= $tahun . "_" . $bulan .  "_" . $tgl;
			
			$data['nik'] 				=$nik_karyawan;
			$data['nama_lengkap'] 		=$this->input->post('nama_karyawan');
			$data['tempat_lahir'] 		=$this->input->post('tempat_lahir');
			$data['tgl_lahir'] 			=$tgl_gabung;
			$data['jenis_kelamin'] 		=$this->input->post('jenis_kelamin');
			$data['alamat'] 			=$this->input->post('alamat');
			$data['telp'] 				=$this->input->post('telpon');
			$data['kode_jabatan'] 		=$this->input->post('kode_jabatan');
			$data['flag'] 				=1;
			$data['foto']				=$this->uploadfoto($nik_karyawan);
			$this->db->insert($this->_table, $data);
			//catetan $data['nama_lengkap']<-(seseuai database) =$this->input->post('nama_karyawan')<-seseuai inputkaryawan;

		}
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}


public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama_lengkap']			= $this->input->post('nama_karyawan');
		$data['tempat_lahir']			= $this->input->post('tempat_lahir');
		$data['tgl_lahir']				= $tgl_gabung;
		$data['jenis_kelamin']			= $this->input->post('jenis_kelamin');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['kode_jabatan']			= $this->input->post('kode_jabatan');
		$data['flag']					= 1;

		if (!empty($_FILES["image"]["name"])) {
			$this->hapusfoto($nik);
			$fotokaryawan	= $this->uploadfoto($nik);
		}else{
			$fotokaryawan	= $this->input->post('foto_old');
		}
		

		$data["foto"]		= $fotokaryawan;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}

	public function delete($nik)
	
		{
			$this->db->where('nik',$nik);
			$this->db->delete($this->_table);
		}

	private function uploadfoto($nik)
	{
		$date_upload				= date('Ymd');
		$config['upload_path']		= './resources/fotokaryawan/';
		$config['allowed_types']	= 'gif|jpg|png|jpeg';
		$config['file_name']		= $date_upload .'_' . $nik;
		$config['overwrite']		= true;
		$config['max_size']			= 1024;//1mb
		//$config['max_width']		= 1024;
		//$config['max_height']		= 768;

		//echo "<pre>";
		//print_r($_FILES["image"]); die();
		//echo "</pre>";

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			$nama_file = $this->upload->data("file_name");
		}else{
			$nama_file	= "default.png";
		}

		return $nama_file;
	}

	private function hapusfoto($nik)
	{
		//cari nama file
		$data_karyawan = $this->detail($nik);
		foreach ($data_karyawan as $data) {
			$nama_file = $data->foto;
		}

		if ($nama_file != "default.png") {
		 	$path = "./resources/fotokaryawan/" . $nama_file;
		 	//bentuk path nya = .//resources/fotokaryawan/20190328_976876.jpeg
		 	return unlink($path);
		 } 
	}

	public function tampilDataKaryawanPagination($perpage, $uri, $data_pencarian)
	{
		// echo "<pre>";
		// print_r($data_pencarian); die();
		// echo "</pre>";
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			$this->db->like('nama_lengkap', $data_pencarian);
		}
		$this->db->order_by('nik','asc');

		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return$get_data->result();
		}else{
			return null;
		}

	}

	public function tombolpagination($data_pencarian)
	{
		//echo "<pre>";
		//print_r($data_pencarian); die();
		//echo "</pre>";
		//cari jmlh data berdasarkan data pencarian
		$this->db->like('nama_lengkap', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		//echo "<pre>";
		//print_r($hasil); die();
		//echo "</pre>";

		//pagination limit
		$pagination['base_url']		= base_url().'Karyawan/listkaryawan/load/';
		$pagination['total_rows']	=$hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;

		//custom pagging configuration
		$pagination['full_tag_open']	= '<div class="pagination">';
		$pagination['full_tag_close']	= '</div>';

		$pagination['first_link']		= 'First Page';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';

		$pagination['last_link']		= 'Last Page';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';

		$pagination['next_link']		= 'Next Page';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';

		$pagination['prev_link']		= 'Prev Page';
		$pagination['prev_tag_open']	= '<span class="prevlink">';
		$pagination['prev_tag_close']	= '</span>';

		$pagination['cur_tag_open']		= '<span class="curlink">';
		$pagination['cur_tag_close']	= '</span>';

		$pagination['num_tag_open']		= '<span class="numlink">';
		$pagination['num_tag_close']	= '</span>';

		$this->pagination->initialize($pagination);

		$hasil_pagination	= $this->tampilDataKaryawanPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian);

		return $hasil_pagination;
	}

} 