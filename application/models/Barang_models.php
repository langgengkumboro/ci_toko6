<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_models extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "barang";

	public function rules(){
		return
		[
					[ 
						'field'=> 'kode_barang',
						'label' => 'Kode Barang',
						'rules' => 'required|max_length[5]',
						'errors' => [
							'requered' => 'kode barang tidak boleh kosong.',
							'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
						]
					],
					[
						'field' => 'nama_barang',
						'label'  => 'Nama Barang',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Nama Barang tidak Boleh kosong.',
						]
					],
					[
						'field' => 'harga_barang',
						'label'  => 'Harga Barang',
						'rules' => 'required|numeric',
						'errors' =>[
							'required' => 'Harga Barang tidak Boleh kosong.',
							'numeric'	=> 'Harga Barang harus angka.',
						]
					]
					//[
					//	'field' => 'kode_jenis',
					//	'label'  => 'Kode Jenis',
					//	'rules' => 'required',
					//	'errors' =>[
					//		'required' => 'Kode Jenis tidak Boleh kosong.',
							
					//	]
					//],
					//[
					//	'field' => 'stok',
					//	'label'  => 'Stok Barang',
					//	'rules' => 'required|numeric',
					//	'errors' =>[
					//		'required' => 'Stok Barang tidak Boleh kosong.',
					//		'numeric'	=> 'Stok Barang harus angka.',
					//	]
					//]
		];
	}

	public function tampilDataBarang()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataBarang2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query(
			"SELECT A. *,  B.nama_jenis FROM " . $this->_table_barang . " AS A
			INNER JOIN `jenis_barang` AS B ON A.kode_jenis = B.kode_jenis");
			return $query->result();
		}

	public function tampilDataBarang3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('kode_barang', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
		public function save()
		{
			
			$data['kode_barang'] =$this->input->post('kode_barang');
			$data['nama_barang'] =$this->input->post('nama_barang');
			$data['harga_barang'] =$this->input->post('harga_barang');
			$data['kode_jenis'] =$this->input->post('kode_jenis');
			$data['flag'] =1;
			$this->db->insert($this->_table, $data);
			//catetan $data['nama_lengkap']<-(seseuai database) =$this->input->post('nama_karyawan')<-seseuai inputkaryawan;

		}
		public function detail($kode_barang)
			{
				//return $query->result();
				$this->db->select('*');
				$this->db->where('kode_barang', $kode_barang);
				$this->db->where('flag', 1);
				$result = $this->db->get($this->_table);
				return $result->result();
			}

		public function update($kode_barang)
	{
		$data['nama_barang']			= $this->input->post('nama_barang');
		$data['harga_barang']			= $this->input->post('harga_barang');
		$data['kode_jenis']				= $this->input->post('kode_jenis');
		$data['flag']					= 1;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);
	}
	public function delete($kode_barang)
	
	{
		$this->db->where('kode_barang',$kode_barang);
		$this->db->delete($this->_table);
	}

public function updateStok($kd_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kd_barang);
        foreach ($cari_stok as $data) {
            $stock=$data->stock;
        }
        
        //proses update stok table barang
        $jumlah_stok     		= $stock + $qty;
        $data_barang['stock']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kd_barang);
        $this->db->update('barang', $data_barang);
    }

public function tampilDataBarangPagination($perpage, $uri, $data_pencarianbarang)
	{
		// echo "<pre>";
		// print_r($data_pencarian); die();
		// echo "</pre>";
		$this->db->select('*');
		if (!empty($data_pencarianbarang)) {
			$this->db->like('nama_barang', $data_pencarianbarang);
		}
		$this->db->order_by('kode_barang','asc');

		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return$get_data->result();
		}else{
			return null;
		}

	}
public function tombolpaginationbarang($data_pencarianbarang)
	{
		//echo "<pre>";
		//print_r($data_pencarian); die();
		//echo "</pre>";
		//cari jmlh data berdasarkan data pencarian
		$this->db->like('nama_barang', $data_pencarianbarang);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		//echo "<pre>";
		//print_r($hasil); die();
		//echo "</pre>";

		//pagination limit
		$pagination['base_url']		= base_url().'Barang/data_barang/load/';
		$pagination['total_rows']	=$hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;




		//custom pagging configuration
		$pagination['full_tag_open']	= '<div class="pagination"><ul>';
		$pagination['full_tag_close']	= '</ul></div>';

		$pagination['first_link']		= 'First Page |';
		$pagination['first_tag_open']	= '<li class="firstlink">';
		$pagination['first_tag_close']	= '</li>';

		$pagination['last_link']		= 'Last Page |';
		$pagination['last_tag_open']	= '<li class="lastlink">';
		$pagination['last_tag_close']	= '</li>';

		$pagination['next_link']		= 'Next Page |';
		$pagination['next_tag_open']	= '<li class="nextlink">';
		$pagination['next_tag_close']	= '</li>';

		$pagination['prev_link']		= 'Prev Page |';
		$pagination['prev_tag_open']	= '<li class="prevlink">';
		$pagination['prev_tag_close']	= '</li>';

		$pagination['cur_tag_open']		= '<li class="curlink">';
		$pagination['cur_tag_close']	= '</li>';

		$pagination['num_tag_open']		= '<li class="numlink">';
		$pagination['num_tag_close']	= '</li>';

		$this->pagination->initialize($pagination);

		$hasil_pagination	= $this->tampilDataBarangPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarianbarang);

		return $hasil_pagination;
	}

} 