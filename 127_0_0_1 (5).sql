-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2019 at 01:22 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stock`) VALUES
('22231', 'Printer Canon M789', 450007, 'JN001', 1, 49),
('BR001', 'Laptop Samsung', 4500000, 'JN001', 1, 253),
('BR007', 'Samsung J2 Pro 2018', 1999000, 'JN002', 1, 7),
('BR012', 'Natrium Hipoklorit', 700000, 'JN004', 1, 0),
('BR013', 'Printer Canon', 600000, 'JN002', 1, 3),
('BR027', 'Software CorelDraw', 2000000, 'JN003', 1, 7),
('BR043', 'Natrium Hipoklorit', 450000, 'JN004', 1, 0),
('BR057', 'Le Mineral', 2500, 'JN001', 1, 1000),
('BR090', 'PC Samsung', 3500000, 'JN002', 1, 0),
('BR097', 'PC LG', 2000000, 'JN002', 1, 25),
('BR099', 'Natrium Hipoklorit', 700000, 'JN001', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('', '', '', 1),
('JB002', 'Admin', 'Operasional', 1),
('JB003', 'Junior Programmer', 'Programmer', 1),
('JB007', 'Staff', 'Ketua', 1),
('JB009', 'Junior Programmer', 'Programmer', 1),
('JB017', 'Admin', 'Operasional', 1),
('JB019', 'Office Boy', 'Oprasional', 1),
('JB021', 'Pegawai', 'Operasional', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat Tulis Kantor', 1),
('JN002', 'Perangkat Keras', 1),
('', '', 1),
('JN003', 'Perangkat Lunak', 1),
('JN004', 'Bahan Kimia', 1),
('JN001', 'Alat Tulis Kantor', 1),
('JN008', 'Minuman', 1),
('JN008', 'Minuman', 1),
('JN021', 'ATM', 1),
('JN002', 'Software', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `foto` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`, `foto`) VALUES
('1704421', 'huhuh', 'jijij', '2019-04-03', 'L', 'jnjn', '0999', 'JB003', 1, ''),
('1902007', 'Rahmad', 'Maluku', '0000-00-00', 'L', 'Maluku Raya', '', 'JB007', 1, '20190329_1902007.jpeg'),
('1902023', 'Usman', 'Lampung', '1994-03-01', 'L', 'Manahan', '0897 1123 238', 'JB003', 1, '20190328_1902023.jpg'),
('1902079', 'Sari', 'Lampung', '1959-04-01', 'P', 'Lampung Barat', '0897 1123 938', 'JB003', 1, '20190402_1902079.jpg'),
('1902083', 'Aan', 'Lampung', '2004-04-02', 'P', 'Cengkareng', '0897 1123 268', 'JB002', 1, '20190402_1902083.jpeg'),
('1902098', 'Sari Puspita ', 'Lampung', '2002-01-01', 'P', 'Jl. Raya Aspal', '0876578855', 'JB003', 1, ''),
('19029099', 'Atta Larik', 'Manado', '1998-03-06', 'L', 'Jl. Mahakam', '0866578991', 'JB003', 1, '20190328_19029099.jpg'),
('190293', 'Ahmad Zaky Syaifudin', 'Sragen', '1986-08-24', 'L', 'Jakarta', '0877716212', 'JB003', 1, '20190328_190293.png'),
('1929', 'Asep Suherman', 'Lebak', '0000-00-00', 'L', 'RSN WADUK SUNTER', '', 'JB003', 1, '20190402_1929.jpg'),
('1982821', 'Uchika', 'Yokohama', '1998-09-10', 'L', 'Jl. Jalur Kuning ', '0877 1728231', 'JB003', 1, ''),
('19829291', 'ucok', 'ddd', '1998-09-11', 'L', 'fffffffff', '22222', 'JB004', 1, ''),
('19921010', 'Sitomarang', 'Sumatera Utara', '1997-03-13', 'L', 'Mekar Wangi Indah Permai', '0815 7818 918', 'JB003', 1, '20190328_19921010.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, '1704421300', 'user@gmail.com', '24c9e15e52afc47c225b757e7bee1f9d', 2, 1),
(2, 'user2', 'user2@gmail.com', '7e58d63b60197ceb55a1c487989a3720', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pejualan-detail`
--

CREATE TABLE `pejualan-detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 59, 'BR001', 2, 1200000, 2400000, 1),
(2, 59, 'BR001', 2, 1200000, 2400000, 1),
(3, 59, 'BR001', 2, 1200000, 2400000, 1),
(4, 59, 'BR001', 2, 1200000, 2400000, 1),
(5, 59, 'BR001', 2, 1200000, 2400000, 1),
(6, 59, 'BR001', 2, 1200000, 2400000, 1),
(7, 60, 'BR001', 7, 1200000, 8400000, 1),
(8, 60, 'BR001', 7, 1200000, 8400000, 1),
(9, 60, 'BR001', 7, 1200000, 8400000, 1),
(10, 60, 'BR001', 7, 1200000, 8400000, 1),
(11, 60, 'BR001', 7, 1200000, 8400000, 1),
(12, 61, 'BR001', 2, 4500000, 9000000, 1),
(13, 61, 'BR001', 2, 4500000, 9000000, 1),
(14, 61, 'BR001', 2, 4500000, 9000000, 1),
(15, 62, '22231', 7, 1200000, 8400000, 1),
(16, 62, '22231', 7, 1200000, 8400000, 1),
(17, 62, '22231', 7, 1200000, 8400000, 1),
(18, 63, '22231', 7, 1200000, 8400000, 1),
(19, 64, 'BR007', 9, 800000, 7200000, 1),
(20, 64, 'BR007', 9, 800000, 7200000, 1),
(21, 65, '22231', 4, 750000, 3000000, 1),
(22, 65, '22231', 4, 750000, 3000000, 1),
(23, 65, '22231', 4, 750000, 3000000, 1),
(24, 65, '22231', 2, 750000, 1500000, 1),
(25, 65, '22231', 2, 750000, 1500000, 1),
(26, 65, '22231', 2, 750000, 1500000, 1),
(27, 65, '22231', 9, 750000, 6750000, 1),
(28, 65, '22231', 9, 750000, 6750000, 1),
(29, 65, '22231', 0, 0, 0, 1),
(30, 65, 'BR001', 80, 7000000, 560000000, 1),
(31, 66, 'BR001', 76, 7500000, 570000000, 1),
(32, 67, '22231', 7, 1200000, 8400000, 1),
(33, 67, '22231', 0, 0, 0, 1),
(34, 67, '22231', 0, 0, 0, 1),
(35, 70, '22231', 7, 1200000, 8400000, 1),
(36, 70, '22231', 7, 1200000, 8400000, 1),
(37, 71, '22231', 2, 1200000, 2400000, 1),
(38, 71, '22231', 2, 1200000, 2400000, 1),
(39, 71, 'BR001', 7, 12000, 84000, 1),
(40, 73, '22231', 3, 1200000, 3600000, 1),
(41, 73, '22231', 3, 1200000, 3600000, 1),
(42, 51, 'BR006', 7, 98000, 98007, 1),
(43, 50, '22231', 7, 1200000, 1200007, 1),
(44, 80, '22231', 7, 1200000, 8400000, 1),
(45, 81, 'BR001', 90, 7500000, 675000000, 1),
(46, 82, 'BR057', 1000, 1500, 1500000, 1),
(47, 83, 'BR097', 25, 3000000, 75000000, 1),
(48, 83, 'BR013', 3, 650000, 1950000, 1),
(49, 84, 'BR007', 7, 1999000, 13993000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `tanggal`, `kode_supplier`, `approved`, `flag`) VALUES
(50, 'TR0977271', '2019-02-21', 'SP002', 0, 0),
(51, 'TR0977279', '2019-02-21', 'SP011', 0, 0),
(53, 'TR0912001', '2019-02-21', 'SP002', 0, 0),
(54, 'TR45', '2019-02-21', 'SP002', 0, 0),
(55, 'TR0977271', '2019-02-21', 'SP009', 0, 0),
(56, 'TR0977271', '2019-02-21', 'SP009', 0, 0),
(58, '123', '2019-02-21', 'SP002', 1, 1),
(59, '123', '2019-02-21', 'SP002', 1, 1),
(60, 'TR0977271', '2019-03-21', 'SP002', 1, 1),
(61, 'TR0977271', '2019-03-21', 'SP002', 1, 1),
(62, 'TR0977277', '2019-03-26', 'SP002', 1, 1),
(63, 'TR7771', '2019-03-26', 'SP002', 1, 1),
(64, 'TR0977276', '2019-03-26', 'SP002', 1, 1),
(65, 'TR0977271', '2019-04-02', 'SP002', 1, 1),
(66, 'TR097755', '2019-04-02', 'SP009', 1, 1),
(67, 'TR0977271', '2019-04-06', 'SP002', 1, 1),
(70, 'TR0977271', '2019-04-06', 'SP002', 1, 1),
(71, 'TR097755', '2019-04-06', 'SP002', 1, 1),
(72, 'TR0977271', '2019-04-08', 'SP002', 1, 1),
(73, 'TR0977279', '2019-04-08', 'SP011', 1, 1),
(74, 'TR0977271', '2019-04-08', 'SP002', 1, 1),
(75, 'TR097755', '2019-04-08', 'SP009', 1, 1),
(76, 'TR0977270', '2019-04-08', 'SP004', 1, 1),
(77, 'TR0977', '2019-04-08', 'SP982', 1, 1),
(78, 'TR20190408', '2019-04-08', 'SP004', 1, 1),
(79, 'TR091', '2019-04-09', 'SP002', 1, 1),
(80, 'TR097', '2019-04-09', 'SP002', 1, 1),
(81, 'TR901', '2019-04-09', 'SP009', 1, 1),
(82, 'TR219', '2019-04-09', 'SP011', 1, 1),
(83, 'TR092', '2019-04-09', 'SP094', 1, 1),
(84, 'TR096', '2019-04-09', 'SP009', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP002', 'PT. Amarta Indah Otsuka', 'Pasuruan 67172, Jawa Timur', '08001687852', 1),
('SP004', 'PT. Senosa Makmur Tbk', 'Jl. Raya Gadog 12', '021 0291 9912', 1),
('SP009', 'Samsung Indonesia', 'Cikarang', '021 7777 9999', 1),
('SP011', 'PT. Garda Terdepan Utama', 'Jl. Mangga Besar Kav. 9A', '021 778 775', 1),
('SP012', 'PT. Garda', 'Jl. Mangga Besar Kav. 9C', '0877 8191 9013', 1),
('SP094', 'PT.Graha Asia Pasific', 'Jl. Pluit Raya No.9D', '0213 991 1198', 1),
('SP982', 'Harapan Seha', 'Jl. Temabaga Mas No.7ABC', '0214 786 443', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `pejualan-detail`
--
ALTER TABLE `pejualan-detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pejualan-detail`
--
ALTER TABLE `pejualan-detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
